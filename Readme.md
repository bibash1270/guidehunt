# Quick Start
  Install **Node.js** and **npm**

  ## Install Cordova
  ````bash
    npm install cordova -g
  ````
  ## Install gulp
  ````bash
    npm install gulp -g
  ````
  ## Install required modules
  ````bash
    npm install
  ````
  ## Run the watcher
  ````bash
    gulp watch
  ````
  For css edit ````./sass/app.scss````